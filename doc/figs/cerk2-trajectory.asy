settings.outformat = "pdf";
unitsize(1cm);
import graph;
import markers;

defaultpen(fontsize(20pt));


// Graph Boundaries
real xmin = -1;
real xmax = 18;

real xleft = 3;
real xright = 12;

real tmin = -1;
real tmax = 7.5;

real tinit = 1.5;
real tfinal = 6;


// Time
real dt = tfinal - tinit;

// Initial Space
real dx = xright - xleft;

// Velocity
real vl = -0.2;
real vr = 0.4;

// Number of Space Gauss Points  (including End Points)
int xng = 5;

// Number of Time Gauss Points (including End Points)
int tng = 4;

// Space Gauss Points
real[] xg = {0.0,
             (1.0 - sqrt(3.0/5.0))/2.0,
             1.0/2.0,
             (1.0 + sqrt(3.0/5.0))/2.0,
             1.0
            };

// Time Gauss Points
real[] tg = {0.0,
             (1-sqrt(1.0/3.0))/2.0,
             (1+sqrt(1.0/3.0))/2.0,
             1.0
            };

// Velocity at Space Gauss Points
real[] vg;

for(int i=0; i < xng; ++i)
{
  vg.push (vl*(1-xg[i]) + vr*xg[i]);
}

// The Axes
xaxis(axis=YEquals(0),xmin=xmin,xmax=xmax,Arrow(2mm),true);
label("$x$", (xmax,0), align=E);
yaxis(axis=XEquals(0),ymin=tmin,ymax=tmax,Arrow(2mm),true);
label("$t$", (0,tmax), align=N);

// Draw Grey Dashed Lines at Gauss Times
for(int i=0; i < tng; ++i)
{
  draw( (xleft - 2, tinit + tg[i]*dt) -- (xright + 3 , tinit + tg[i]*dt),
        dashed+gray );
}

defaultpen(fontsize(15pt));

// Time Labels
label("$t_{n}$", (xright + 3,tinit), align=E);
label("$\tau_{1}$", (xright + 3,tinit + tg[1]*dt), align=E);
label("$\tau_{2}$", (xright + 3,tinit + tg[2]*dt), align=E);
label("$t_{n+1}$", (xright + 3,tfinal), align=E);

// Evolution of Cell Points in Time
// Boundary Points
draw((xleft, tinit) -- (xleft + dt*vl, tfinal));
draw((xright, tinit) -- (xright+ dt*vr,tfinal));

// Evolution of Cell Points in Time
// Interior Points
for(int i=1; i < 4; ++i)
{
  draw(    (xleft + xg[i]*dx + tg[0]*dt*vg[i], tinit + tg[0]*dt)
        -- (xleft + xg[i]*dx + tg[3]*dt*vg[i], tinit + tg[3]*dt),
           dashed+gray
      );
}

draw(    (xleft + xg[2]*dx + tg[0]*dt*vg[2], tinit + tg[0]*dt)
        -- (xleft + xg[2]*dx + tg[2]*dt*vg[2], tinit + tg[2]*dt),
           red+linewidth(2)
      );
draw( (xleft+xg[2]*dx + tg[2]*dt*vg[2], tinit + tg[2]*dt),
      marker(scale(2.5)*unitcircle,Fill));
// Horizontal Boundaries for the Cell
draw((xleft,tinit) -- (xright,tinit));
draw((xleft + dt*vl,tfinal) -- (xright + dt*vr, tfinal));

// X Labels
label("$i - \frac{1}{2}$", (xleft, tinit-0.25), align=S-0.5);
label("$x_0$", (xleft + xg[1]*dx, tinit-0.25), align=S);
label("$x_q(t_n)$", (xleft + xg[2]*dx, tinit-0.25), align=S);
label("$x_q(\tau_2)$", (xleft + xg[2]*dx + tg[2]*dt*vg[2],  tinit + tg[2]*dt),
align=NE);
label("$x_2$", (xleft + xg[3]*dx, tinit-0.25), align=S-0.25);
label("$i + \frac{1}{2}$", (xright, tinit-0.25), align=S+0.5);

// Interior Points at t0
for(int i=1; i < 4; ++i)
{
  draw( (xleft+xg[i]*dx + tg[0]*dt*vg[i], tinit + tg[0]*dt),
    scale(3)*marker(cross(4)));
}


//for (int j=1; j < 3; ++j)
//{
//  // Endpoints at t1
//  draw((xleft + xg[0]*dx + tg[j]*dt*vg[0], tinit + tg[j]*dt),
//    marker(scale(4)*polygon(4)));
//
//  draw((xleft + xg[4]*dx + tg[j]*dt*vg[4], tinit + tg[j]*dt),
//    marker(scale(4)*polygon(4)));
//
//  // Interior Points at t1
//  for(int i=1; i < 4; ++i)
//  {
//    draw( (xleft+xg[i]*dx + tg[j]*dt*vg[i], tinit + tg[j]*dt),
//      marker(scale(2.5)*unitcircle,Fill));
//  }
//}
