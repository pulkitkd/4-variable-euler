settings.outformat = "pdf";
unitsize(1.5cm);
import graph;
import markers;

defaultpen(fontsize(60pt));
size(20,20);

real xlim = 5;
real ylim = 4;

draw((0,0) -- (xlim,0), linewidth(3));
draw((xlim,0) -- (xlim,ylim), linewidth(3));
draw((xlim,ylim) -- (0,ylim), linewidth(3));
draw((0,ylim) -- (0,0), linewidth(3));
draw((0,2.75) -- (xlim,2.75), linewidth(3));
draw((0,1.25) -- (xlim,1.25), linewidth(3));
label("$v_1$", (2.75, 3.25), align = E);
draw((2.75,3.25) -- (2,3.25), EndArrow);
label("$v_2$", (2.75, 2), align = E);
draw((2.75,2) -- (2,2), BeginArrow);
label("$v_1$", (2.75, 0.5), align = E);
draw((2.75,0.5) -- (2,0.5), EndArrow);
