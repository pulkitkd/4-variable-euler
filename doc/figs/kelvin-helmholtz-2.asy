settings.outformat = "pdf";
unitsize(1.5cm);
import graph;
import markers;

defaultpen(fontsize(60pt));
size(20,20);

real xlim = 5;
real ylim = 4;

draw((0,0) -- (xlim,0), linewidth(2));
draw((xlim,0) -- (xlim,ylim), linewidth(2));
draw((xlim,ylim) -- (0,ylim), linewidth(2));
draw((0,ylim) -- (0,0), linewidth(2));
draw((0,2.75) -- (xlim,2.75), linewidth(2));
draw((0,1.25) -- (xlim,1.25), linewidth(2));
label("$v_1 + v$", (2.25, 3.25), align = E);
draw((2.25,3.25) -- (1.5,3.25), EndArrow);
label("$v_2 + v $", (2.25, 2), align = E);
draw((2.25,2) -- (1.5,2), BeginArrow);
label("$v_1 + v $", (2.25, 0.5), align = E);
draw((2.25,0.5) -- (1.5,0.5), EndArrow);
