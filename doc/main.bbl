\begin{thebibliography}{1}

\bibitem{Gassner20114232}
{\sc G.~Gassner, M.~Dumbser, F.~Hindenlang, and C.-D. Munz}, {\em Explicit
  one-step time discretizations for discontinuous galerkin and finite volume
  schemes based on local predictors}, Journal of Computational Physics, 230
  (2011), pp.~4232 -- 4247.

\bibitem{Johnsen:2010:AHM:1670833.1670927}
{\sc E.~Johnsen, J.~Larsson, A.~V. Bhagatwala, W.~H. Cabot, P.~Moin, B.~J.
  Olson, P.~S. Rawat, S.~K. Shankar, B.~Sj\"{o}green, H.~C. Yee, X.~Zhong, and
  S.~K. Lele}, {\em Assessment of high-resolution methods for numerical
  simulations of compressible turbulence with shock waves}, J. Comput. Phys.,
  229 (2010), pp.~1213--1237.

\bibitem{Luo2004304}
{\sc H.~Luo, J.~D. Baum, and R.~L{\"o}hner}, {\em On the computation of
  multi-material flows using {ALE} formulation}, Journal of Computational
  Physics, 194 (2004), pp.~304 -- 328.

\bibitem{Mandal2012148}
{\sc J.~Mandal and V.~Panwar}, {\em Robust {HLL}-type {R}iemann solver capable
  of resolving contact discontinuity}, Computers \& Fluids, 63 (2012), pp.~148
  -- 164.

\bibitem{Owren:1992:DEC:141072.141096}
{\sc B.~Owren and M.~Zennaro}, {\em Derivation of efficient, continuous,
  explicit runge-kutta methods}, SIAM J. Sci. Stat. Comput., 13 (1992),
  pp.~1488--1501.

\bibitem{Roe1981357}
{\sc P.~L. Roe}, {\em Approximate {R}iemann solvers, parameter vectors, and
  difference schemes}, Journal of Computational Physics, 43 (1981), pp.~357 --
  372.

\bibitem{springel2010pur}
{\sc V.~Springel}, {\em E pur si muove: Galilean-invariant cosmological
  hydrodynamical simulations on a moving mesh}, Monthly Notices of the Royal
  Astronomical Society, 401 (2010), pp.~791--851.

\bibitem{Zhang:2010:PHO:1864819.1865066}
{\sc X.~Zhang and C.-W. Shu}, {\em On positivity-preserving high order
  discontinuous galerkin schemes for compressible euler equations on
  rectangular meshes}, J. Comput. Phys., 229 (2010), pp.~8918--8934.

\end{thebibliography}
