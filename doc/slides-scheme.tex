\begin{frame}[allowframebreaks]
\frametitle{Mesh}

\begin{itemize}

\item Partition the domain into disjoint cells
\begin{equation*}
  \Omega(t) = \bigcup\limits_{i=0}^N C_j(t),\qquad C_j(t) =
\left(x_{j-\frac{1}{2}}(t), x_{j+\frac{1}{2}}(t)\right)
 \end{equation*}
\begin{center}
\includegraphics[width=0.8\textwidth]{figs/1dgrid}
\end{center}
 \item Discrete time levels  given by $\{t_n\}$
 \item Time steps given by $\Delta t_n = t_{n+1} - t_n$

\item Velocity of cell boundaries are assumed constant in a time step
    $\Delta t_n$
    \begin{equation*}
    \dd{}{t}x_\jph(t) = w_{j+\frac{1}{2}}(t) = w_{j+\frac{1}{2}}^n,\qquad t_n \leq t \leq t_{n+1}
    \end{equation*}
  \begin{equation*}
    \Longrightarrow x_{j+\frac{1}{2}}(t) = x_{j+\frac{1}{2}}^n
    + (t - t_n)w_{j+\frac{1}{2}}^n,\qquad t_n\leq t \leq t_{n+1}
  \end{equation*}

\item Center of the cell $x_j(t)$ and length $h_j(t)$ are given by
  \begin{align*}
    x_{j}(t) &= \frac{1}{2}\left(x_{j-\frac{1}{2}}(t)
    + x_{j+\frac{1}{2}}(t)\right),
    & h_j(t) &= x_{j+\frac{1}{2}}(t) - x_{j-\frac{1}{2}}(t)
  \end{align*}

\item Velocity at the interior points is given by linear interpolation
\begin{equation*}
w(x,t) = \frac{x_{j + \frac{1}{2}}(t) - x}{h_j(t)} w_{j - \frac{1}{2}}^n +
\frac{x -  x_{j + \frac{1}{2}}(t)}{h_j(t)} w_{j + \frac{1}{2}}^n
\end{equation*}

\item Example of moving cell
\begin{center}

\includegraphics[width=0.7\textwidth,keepaspectratio=true]{figs/moving-mesh.pdf}
 % cerk2.pdf: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
%  \caption{Evolution of Mesh Velocity}
\end{center}

\end{itemize}

\end{frame}
%--------------------------------------------------------------------------------------

\begin{frame}[allowframebreaks]
\frametitle{Solution Space}

\begin{minipage}{.4\textwidth}
\begin{center}
\includegraphics[width=0.8\textwidth]{figs/dg_sol}
\end{center}
\end{minipage}% This must go next to `\end{minipage}`
\begin{minipage}{.6\textwidth}
  \begin{itemize}
\item Solution is approximated by piecewise polynomials.
\item allowed to be discontinuous at cell boundaries
\end{itemize}
\end{minipage}
\begin{itemize}
\item For degree $k \geq 0$, the solution in the $j$-th cell is given by
\begin{equation*}
 \bfu_h(x,t) = \sum\limits_{m=0}^k \bfu_{j,m}(t) \varphi(x,t)
\end{equation*}
\begin{equation*}
 \varphi_m(x,t) = \widehat{\varphi}_m(\xi) = \sqrt{2m+1}P_m(\xi), \qquad
\xi(x,t)=\frac{x - x_j(t)}{\frac{1}{2}h_j(t)}
\end{equation*}
\item orthogonality property
\begin{equation*}
\int_{x_{j - \frac{1}{2}}(t)}^{x_{j + \frac{1}{2}}(t)} \varphi_l(x,t)
\varphi_m(x,t) \rmd x = h_j(t) \delta_{lm}
\end{equation*}
\item This allows us to write the expression for the ``moments'' as
\begin{eqnarray*}
\bfu_{j,m}(t) =  \int_{x_\jmh(t)}^{x_\jph(t)} \con_h(x,t) \basis_l(x,t) \ud x
\end{eqnarray*}
  \end{itemize}
\end{frame}
%--------------------------------------------------------------------------------------
\begin{frame}[allowframebreaks]
\frametitle{Derivation of the ALE-DG scheme}
Introduce the change of variable $(x,t) \to (\xi,\tau)$ by
\[
\tau = t, \qquad \xi = \frac{x - x_j(t)}{\half h_j(t)}
\]
Calculate the rate of change of moments of the solution starting from
\begin{eqnarray*}
\dd{}{t} \int_{x_\jmh(t)}^{x_\jph(t)} \con_h(x,t) \basis_l(x,t) \ud x
&=& \dd{}{\tau} \int_{-1}^{+1} \con_h(\xi,\tau) \rbasis_l(\xi) \half h_j(\tau) \ud\xi \\
&=& \half \int_{-1}^{+1} \left[ h_j(\tau) \df{\con_h}{\tau} + \con_h \dd{h_j}{\tau} \right] \rbasis_l(\xi) \ud\xi
\end{eqnarray*}
But we have
\[
\df{\con_h}{\tau}(\xi,\tau) = \df{\con_h}{t}(x,t) + w(x,t) \df{\con_h}{x}(x,t)
\]
and
\[
\dd{h_j}{\tau} = w_\jph - w_\jmh = h_j \df{w}{x} \qquad \textrm{since $w(x,t)$ is linear in $x$}
\]
{\small
\begin{eqnarray*}
\dd{}{t} \int_{x_\jmh(t)}^{x_\jph(t)} \con_h(x,t) \basis_l(x,t) \ud x
&=& \int_{-1}^{+1} \left[ \df{\con_h}{t} + w \df{\con_h}{x} + \con_h \df{w}{x} \right] \rbasis_l(\xi) \half h_j \ud \xi \\
&=& \int_{x_\jmh(t)}^{x_\jph(t)} \left[ - \df{\fl(\con_h)}{x} + \df{}{x}(w \con_h) \right] \basis_l(x,t) \ud x
\end{eqnarray*}
}
Define the flux
\[
\mfl(\con,w) = \fl(\con) - w \con
\]
Performing an integration by parts in the $x$ variable, we obtain
{\footnotesize
\begin{eqnarray*}
\dd{}{t} \int_{x_\jmh(t)}^{x_\jph(t)} \con_h(x,t) \basis_l(x,t) \ud x
&=& \int_{x_\jmh(t)}^{x_\jph(t)} \mfl(\con_h,w) \df{}{x}\basis_l(x,t) \ud x \\
&&  + \nfl_\jmh(\con_h(t)) \basis_l(x_\jmh^+,t) - \nfl_\jph(\con_h(t)) \basis_l(x_\jph^-,t)
\end{eqnarray*}
}
where we have introduced the numerical flux
\[
\nfl_\jph(\con_h(t)) = \nfl( \con_\jph^-, \con_\jph^+, w_\jph)
\]
Integrating over the time interval $(t_n, t_{n+1})$ we obtain
\begin{eqnarray*}
h_j^{n+1} \con_{j,l}^{n+1} &=& h_j^n \con_{j,l}^n + \int_{t_n}^{t_{n+1}} \int_{x_\jmh(t)}^{x_\jph(t)} \mfl(\con_h,w) \df{}{x}\basis_l(x,t) \ud x \ud t \\
&&  + \int_{t_n}^{t_{n+1}} [\nfl_\jmh(t) \basis_l(x_\jmh^+,t) - \nfl_\jph(t) \basis_l(x_\jph^-,t)] \ud t
\end{eqnarray*}
This has an implicit nature; $\con_h$ is known only at $t=t_n$ but we need it over the interval $[t_n, t_{n+1}]$

Assume that we can get a {\em predicted solution} $\conp_h$; then using quadratures, the fully discrete scheme
{\small
\begin{eqnarray*}
h_j^{n+1} \con_{j,l}^{n+1} &=& h_j^n \con_{j,l}^n + \Delta t_n \sum_r \wt_r h_j(\tau_r) \sum_q \wx_q\mfl(\conp_h(x_q,\tau_r),w(x_q,\tau_r)) \df{}{x}\basis_l(x_q,\tau_r) \\
&&  + \Delta t_n \sum_r \wt_r [\nfl_\jmh(\conp_h(\tau_r)) \basis_l(x_\jmh^+,\tau_r) - \nfl_\jph(\conp_h(\tau_r)) \basis_l(x_\jph^-,\tau_r)]
\end{eqnarray*}
}
\[
\tau_r, \wt_r = \textrm{nodes and weights for time qudrature}
\]
\[
x_q, \wx_q = \textrm{nodes and weights for spatial quadrature}
\]
Spatial quadrature: use $q=k+1$ point Gauss quadrature.

Time quadrature: use mid-point rule for $k=1$, two point Gauss quadrature for $k=2,3$, etc.
\end{frame}
%--------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Mesh velocity}

\begin{itemize}

\item Mesh velocity must be close to the local fluid velocity

\item simple choice is to take an average
\[
\tilde{w}_\jph^n = \half[ v(x_\jph^-,t_n) + v(x_\jph^+, t_n)]
\]

\item perform some smoothing of the mesh velocity, e.g.,
\[
w_\jph^n = \frac{1}{3}( \tilde{w}_{j-\half}^n + \tilde{w}_\jph^n + \tilde{w}_{j+\frac{3}{2}}^n)
\]

\end{itemize}

\end{frame}
%--------------------------------------------------------------------------------------
\begin{frame}[allowframebreaks]
\frametitle{Predictor via Taylor expansion}
\vspace{-5mm}
\begin{center}
 \includegraphics[width=0.8\textwidth]{figs/cerk2-trajectory}
\end{center}
The Taylor expansion around $(X_q,t_n)$ is
\begin{eqnarray*}
\con(x_q,\tau_r) &=& \con(X_q,t_n) + (\tau_r - t_n) \df{\con}{t}(X_q,t_n) + (x_q - X_q) \df{\con}{x}(X_q,t_n) \\
&& + O(\tau_r-t_n)^2 + O(x_q-X_q)^2
\end{eqnarray*}
and hence the predicted solution is
\[
\conp(x_q,\tau_r) = \con_h(X_q,t_n) + (\tau_r - t_n) \df{\con_h}{t}(X_q,t_n) + (x_q - X_q) \df{\con_h}{x}(X_q,t_n)
\]
Using the conservation law, the time derivative is written as $\df{\con}{t} = -\df{\fl}{x} = - A \df{\con}{x}$ so that predictor is given by
\[
\conp_h(x_q,\tau_r) = \con_h^n(X_q) - (\tau_r - t_n) \left[ A(\con_h^n(X_q)) - w_q I \right] \df{\con_h^n}{x}(X_q)
\]
The above predictor is used for the case of polynomial degree $k=1$.

\vspace{5mm}

This procedure can be extended to higher orders by including more terms in the Taylor expansion but the algebra becomes complicated.

\end{frame}
%--------------------------------------------------------------------------------------
\begin{frame}[allowframebreaks]
\frametitle{Predictor using Runge-Kutta}

Idea: Apply RK scheme to obtain solution in $[t_n, t_{n+1}]$

\vspace{2mm}

Choose a set of $(k+1)$ distinct nodes, e.g., Gauss-Legendre or Gauss-Lobatto nodes, which uniquely define the polynomial of degree $k$.

\vspace{2mm}

Nodes are moving with velocity $w(x,t)$, the time evolution of the solution at $x=x_m$ is governed by
\begin{eqnarray*}
\dd{\conp_m}{t} &=& \df{}{t}\conp_h(x_m,t) + w(x_m,t) \df{}{x}\conp_h(x_m,t) \\
&=& -\df{}{x}\fl(\conp_h(x_m,t)) + w(x_m,t) \df{}{x} \conp_h(x_m,t) \\
&=& -[ A(\conp_m(t)) - w_m(t) I] \df{}{x}\conp_h(x_m,t) =: \rhs_m(t)
\end{eqnarray*}
with initial condition
\[
\conp_m(t_n) = \con_h(x_m, t_n) = \con_h^n(x_m)
\]
Using a Runge-Kutta scheme of sufficient order, we will approximate the solution at these nodes as
\[
\conp_m(t) = \con_h(x_m, t_n) + \sum_{s=1}^{n_s} b_s((t-t_n)/\Delta t_n) \rhs_{m,s}, \quad t \in [t_n,t_{n+1}), \quad m=0,1,\ldots,k
\]
\[
\rhs_{m,s} = \rhs_m(t_n + \tau_s), \qquad \tau_s = \textrm{stage time}
\]

\begin{center}
\includegraphics[width=0.6\textwidth]{figs/cerk2} \\
Quadrature points for third order scheme
\end{center}

Once the predictor is computed as above, it must be evaluated at the quadrature point $(x_q, \tau_r)$ as follows. For each time quadrature point $\tau_r \in [t_n, t_{n+1}]$,
\begin{enumerate}
\item Compute nodal values  $\conp_m(\tau_r)$, $m=0,1,\ldots,k$
\item Convert nodal values to modal coefficients $\con_{m,r}$, $m=0,1,\ldots,k$
\item Evaluate predictor $\conp_h(x_q,\tau_r) = \sum_{m=0}^k \con_{m,r} \basis_m(x_q, \tau_r)$
\end{enumerate}
The predictor is also computed at the cell boundaries using the above procedure.

\end{frame}
%--------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Limiter}

\begin{itemize}

\item Discontinuous solutions obtained from high order schemes suffer from numerical oscillations: loss of TVD property

\item Post process the DG solution with a TVD or TVB limiter (Cockburn \& Shu)

\item To make density/pressure postive, apply positivity limiter of Zhang \& Shu

\end{itemize}

\end{frame}
%--------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Grid coarsening}

\begin{itemize}

\item Grid cells can become small in size, e.g., around shocks

\item Time step is reduced due to CFL condition

\item If $h_j^n < h_{min}$, then merge this cell with one of its neighbouring cells. Transfer solution by $L^2$ projection.

\end{itemize}

\end{frame}
%--------------------------------------------------------------------------------------
\begin{frame}
\frametitle{Choosing the time step}

\begin{itemize}

\item Geometrical constraint: cell size must not change by more than a fraction $\beta$
\[
(1-\beta) h_j^n \le h_j^{n+1} \le (1+\beta) h_j^n \qquad \textrm{e.g., } \beta = 0.1
\]
\[
\Longrightarrow \Delta t_n \le \frac{\beta h_j^n}{|w_\jph^n - w_\jmh^n|}
\]

\item First order scheme with Rusanov flux is positive if
\begin{equation*}
\Delta t_n \le \Delta t_n^{(1)} := \min_j\left\{ \frac{(1-\thalf\beta) h_j^n}{\thalf(\lambda_\jmh^n + \lambda_\jph^n)},  \frac{\beta h_j^n}{|w_\jph^n - w_\jmh^n|} \right\}
\end{equation*}

\item In general, when degree $k$ polynomials are used
\[
\Delta t_n = \frac{\cfl}{2k+1} \Delta t_n^{(1)}, \qquad \cfl \approx 0.5
\]

\end{itemize}

\end{frame}
