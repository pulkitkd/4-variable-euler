\documentclass[preprint,12pt]{elsarticle}

\usepackage{graphicx}	
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{bm}
%\usepackage{lineno}

\newcommand{\ud}{\textrm{d}}
\newcommand{\dd}[2]{\frac{\ud #1}{\ud #2}}
\newcommand{\df}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\re}{\mathbb{R}}

\newcommand{\con}{\bm{u}}
\newcommand{\fl}{\bm{f}}
\newcommand{\mfl}{\bm{g}}
\newcommand{\nfl}{\hat{\mfl}}
\newcommand{\conp}{\bm{U}}
\newcommand{\pv}{\bm{q}} % Roe parameter vector
\newcommand{\rv}{q} % relative velocity
\newcommand{\arv}{\bar{\rv}} % average relative velocity
\newcommand{\ac}{\bar{c}}
\newcommand{\rhs}{\bm{K}}

\newcommand{\half}{\frac{1}{2}}
\newcommand{\thalf}{\tfrac{1}{2}}
\newcommand{\jmh}{{j - \half}}
\newcommand{\jph}{{j + \half}}
\newcommand{\basis}{\varphi}
\newcommand{\rbasis}{\hat{\varphi}}

\newcommand{\wt}{\theta}
\newcommand{\wx}{\eta}

\newcommand{\cfl}{\textrm{CFL}}

\newtheorem{theorem}{Theorem}
%\theoremstyle{remark}
\newtheorem{remark}{Remark}

\journal{Journal of Computational Physics}

%\linenumbers
\begin{document}

\begin{frontmatter}

\title{Arbitrary Lagrangian-Eulerian discontinuous Galerkin method for 1-D Euler equations}
\author{Praveen Chandrashekar\fnref{1}}
\author{Jayesh Badwaik}
\fntext[1]{Corresponding author, email: {\tt praveen@tifrbng.res.in}}
%\ead{praveen@tifrbng.res.in, jayesh@math.tifrbng.res.in}
\address{TIFR Center for Applicable Mathematics, Bangalore-560065, India}

\begin{abstract}
We propose an explicit in time discontinuous Galerkin scheme on moving grids using the arbitrary Lagrangian-Eulerian approach for one dimensional Euler equations. The grid is moved with a velocity which is close to the local fluid velocity which considerably reduces the numerical dissipation in the Riemann solvers. Local grid refinement and coarsening are performed to maintain the mesh quality and avoid very small or large cells. Several test cases are provided to demonstrate the accuracy of the proposed scheme.
\end{abstract}

\begin{keyword}
Discontinuous Galerkin method, moving meshes, ALE, Euler equations
\end{keyword}

\end{frontmatter}

%-------------------------------------------------------------------------------
\section{Introduction}
Finite volume schemes based on exact or approximate Riemann solvers can be used for solving hyperbolic conservation laws like the Euler equations governing compressible flows. These schemes are able to compute discontinuous solutions in a stable manner since they have implicit dissipation built into them due to the upwind nature of the schemes. Higher order schemes are constructed following a reconstruction approach combined with a high order time integration scheme. Discontinuous Galerkin methods can be considered as higher order generalizations of finite volume methods which also make use of Riemann solver technology but do not need a reconstruction step since they evolve a polynomial solution inside each cell. While these methods are formally high order accurate on smooth solutions, they can introduce too much numerical dissipation in some situations. Springel~\cite{springel2010pur} gives the example of a Kelvin-Helmholtz instability in which adding a large  constant velocity to both states leads to suppresion of the instability due to excessive numerical dissipation. This behaviour is attributed to the fact that fixed grid methods based on upwind schemes are not Galilean invariant. Upwind schemes, even when they are formally high order accurate, are found to be too dissipative when applied to turbulent flows~\cite{Johnsen:2010:AHM:1670833.1670927} since the numerical viscosity can overwhelm the physical viscosity.

For the linear convection equation $u_t + a u_x=0$, the upwind scheme has the modified partial differential equation
\[
\df{u}{t} + a \df{u}{x} = \half|a|h (1 - \nu) \df{^2 u}{x^2} + O(h^2), \qquad \nu = \frac{|a| \Delta t}{h}
\]
which shows that the numerical dissipation is proportional to $|a|$. In case of Euler equations solved with a Riemann solver, e.g., the Roe scheme, the numerical dissipation would be proportional to the absolute values of the eigenvalues, e.g., $|v|$ where $v$ is the fluid velocity. This viscosity is not Galilean invariant since the fluid velocity depends on the coordinate frame adopted for the description of the flow. Adding a large translational velocity will increase the numerical viscosity and reduce the accuracy of the numerical solution. This problem can be eliminated if the grid moves along with the flow as in Lagrangian methods where the equations are written in Lagrangian coordinates. However pure Lagrangian methods encounter the issue of large grid deformations that occur in highly sheared flows as in the Kelvin-Helmholtz problem. A more practical approach is to use arbitrary Lagrangian-Eulerian approach where the mesh velocity can be chosen to be close to the local fluid velocity but may be regularized to maintain the mesh quality. Even in the ALE approach, it may be necessary to perform some local remeshing to prevent the grid quality from degrading. In~\cite{springel2010pur}, the mesh is regenerated after every time step which allows it to maintain good mesh quality even when the fluid undergoes large shear deformation.

Traditionally, ALE methods have been used for problems involving moving boundaries as in wing flutter, store separation and other problems involving fluid structure interaction. In these applications, the main reason to use ALE is not to minimize the dissipation in upwind schemes and hence the grid velocities are chosen purely based on boundary motion and with the view to maintain good mesh quality.

ALE schemes have been used to compute multi-material flows as in~\cite{Luo2004304}, since they are useful to accurately track the material interface.

In the present work, we consider only the one dimensional problem since the multi-dimensional cases involve other issues related to mesh quality. We propose an explicit discontinuous Galerkin scheme that is conservative on moving meshes and automatically satisfies the geometric conservation law. The scheme is a single step method which is achieved by using a predictor as in~(REF). Apart from the geometric complexity, the proposed scheme can be extended to multiple dimensions.
%-------------------------------------------------------------------------------
\section{Euler equations}
The Euler equations represent conservation of mass, momentum and energy, and can be written as a system of conservation laws of the form
\begin{equation}
\df{\con}{t} + \df{\fl(\con)}{x} = 0
\label{eq:claw}
\end{equation}
where $\con$ is the vector of conserved variables and $\fl(\con)$ are the corresponding fluxes given by
\[
\con = \begin{bmatrix}
\rho \\ \rho v \\ E \end{bmatrix}, \qquad \fl(\con) = \begin{bmatrix}
\rho v \\ p + \rho v^2 \\ \rho H v \end{bmatrix}
\]
In the above expressions, $\rho$ is the density, $v$ is the velocity, $p$ is the pressure and $E$ is the total energy per unit volume, which for an ideal gas is given by $E = p/(\gamma-1) + \rho v^2/2$, with $\gamma > 1$ being the ratio of specific heats at constant pressure and volume, and $H=(E+p)/\rho$ is the enthalpy. The Euler equations form a hyperbolic system; the flux Jacobian $A(\con) = \fl'(\con)$ has real eigenvalues and linearly independent eigenvectors. The eigenvalues are $v-c, \ v, \ v+c$ where $c=\sqrt{\gamma p/\rho}$ is the speed of sound and the corresponding eigenvectors are given by
\[
r_1 = \begin{bmatrix}
1 \\ v-c \\ H - vc \end{bmatrix}, \qquad
r_2 = \begin{bmatrix}
1 \\ v \\ \half v^2 \end{bmatrix}, \qquad r_3 = \begin{bmatrix}
1 \\ v + c \\ H + vc \end{bmatrix}
\]
The hyperbolic property implies that $A$ can be diagonalized as $A = R \Lambda R^{-1}$ where $R$ is the matrix formed by the eigenvectors and $\Lambda$ is the diagonal matrix of eigenvalues.
%\subsection{}
%-------------------------------------------------------------------------------
\section{Discontinuous Galerkin scheme}
%-------------------------------------------------------------------------------
\subsection{Mesh and solution space}

Consider a partition of the domain into disjoint cells with the $j$'th cell being denoted by  $C_j(t) = (x_\jmh(t), x_\jph(t))$. As the notation shows, the cell boundaries are time dependent which means that the cell is moving in some specified manner. The time levels are denoted by $t_n$ with the time step $\Delta t_n = t_{n+1} - t_n$. The boundaries of the cells move with a constant velocity in the time interval $(t_n, t_{n+1})$ given by
\[
w_\jph(t) = w_\jph^n, \qquad t_n < t < t_{n+1}
\]
Hence the location of the cell boundaries is given by
\[
x_\jph(t) = x_\jph^n +  (t - t_n) w_\jph^n, \qquad t_n \le t \le t_{n+1}
\]
Let $x_j(t)$ and $h_j(t)$ denote the center of the cell and its length, i.e.,
\[
x_j(t) = \half( x_\jmh(t) + x_\jph(t)), \qquad h_j(t) = x_\jph(t) - x_\jmh(t)
\]
Let $w(x,t)$ be the linear interpolation of the mesh velocity which is given by
\[
w(x,t) = \frac{x_\jph(t) - x}{h_j(t)} w_\jmh^n + \frac{x - x_\jmh(t)}{h_j(t)} w_\jph^n, \qquad x \in C_j(t), \quad t \in (t_n, t_{n+1})
\]
We will approximate the solution by piecewise polynomials which are allowed to be discontinuous across the cell boundaries. For a given degree $k \ge 0$, the solution in the $j$'th cell is given by
\[
\con_h(x,t) = \sum_{m=0}^k \con_{j,m}(t) \basis_m(x,t)
\]
where $\con_{j,m} \in \re^3$ are the degrees of freedom associated with the $j$'th cell. The basis functions $\basis_m$ are defined in terms of Legendre polynomials
\[
\basis_m(x,t) = \rbasis_m(\xi) = \sqrt{2m+1} P_m(\xi), \qquad \xi = \frac{x - x_j(t)}{\half h_j(t)}
\]
where $P_m :[-1,+1] \to \re$ is the Legendre polynomial of degree $m$. The above definition implies the following orthogonality property
\begin{equation}
\int_{x_\jmh(t)}^{x_\jph(t)} \basis_l(x,t) \basis_m(x,t) \ud x = h_j(t) \delta_{lm}
\label{eq:ortho}
\end{equation}
Thus we will sometimes also write the solution in the $j$'th cell in terms of the reference coordinates $\xi$ as
\[
\con_h(\xi,t) = \sum_{m=0}^k \con_{j,m}(t) \rbasis_m(\xi)
\]
%-------------------------------------------------------------------------------
\subsection{Derivation of the scheme}
In order to derive the scheme, let us introduce the change of variable $(x,t) \to (\xi,\tau)$ by
\[
\tau = t, \qquad \xi = \frac{x - x_j(t)}{\half h_j(t)}
\]
For any $0 \le l \le k$, we now calculate the rate of change of the $l$'th moment of the solution starting from
\begin{eqnarray*}
\dd{}{t} \int_{x_\jmh(t)}^{x_\jph(t)} \con_h(x,t) \basis_l(x,t) \ud x 
&=& \dd{}{\tau} \int_{-1}^{+1} \con_h(\xi,\tau) \rbasis_l(\xi) \half h_j(\tau) \ud\xi \\
&=& \half \int_{-1}^{+1} \left[ h_j(\tau) \df{\con_h}{\tau} + \con_h \dd{h_j}{\tau} \right] \rbasis(\xi) \ud\xi
\end{eqnarray*}
wherein we used the above change of variables. But we also have the inverse transform
\[
t = \tau, \qquad x = x_j(\tau) + \frac{\xi}{2} h_j(\tau)
\]
and hence
\[
\df{t}{\tau} = 1, \qquad \df{x}{\tau} = \dd{x_j}{\tau} + \frac{\xi}{2} \dd{h_j}{\tau} = \frac{1}{2}(w_\jmh + w_\jph) + \frac{\xi}{2}(w_\jph - w_\jmh) = w(x,t)
\]
Using this we can show that
\[
\df{\con_h}{\tau}(\xi,\tau) = \df{\con_h}{t}(x,t) + w(x,t) \df{\con_h}{x}(x,t)
\]
Moreover
\[
\dd{h_j}{\tau} = w_\jph - w_\jmh = h_j \df{w}{x}
\]
since $w(x,t)$ is linear in $x$. Hence
\begin{eqnarray*}
\dd{}{t} \int_{x_\jmh(t)}^{x_\jph(t)} \con_h(x,t) \basis_l(x,t) \ud x 
&=& \int_{-1}^{+1} \left[ \df{\con_h}{t} + w \df{\con_h}{x} + \con_h \df{w}{x} \right] \rbasis(\xi) \half h_j \ud \xi \\
&=& \int_{x_\jmh(t)}^{x_\jph(t)} \left[ - \df{\fl}{x} + \df{}{x}(w \con_h) \right] \basis_l(x,t) \ud x
\end{eqnarray*}
where we have transformed back to the physical coordinates and made use of the conservation law~(\ref{eq:claw}). Define the flux
\[
\mfl(\con,w) = \fl(\con) - w \con
\]
Performing an integration by parts in the $x$ variable, we obtain
\begin{eqnarray*}
\dd{}{t} \int_{x_\jmh(t)}^{x_\jph(t)} \con_h(x,t) \basis_l(x,t) \ud x 
&=& \int_{x_\jmh(t)}^{x_\jph(t)} \mfl(\con_h,w) \df{}{x}\basis_l(x,t) \ud x \\
&&  + \nfl_\jmh(\con_h(t)) \basis_l(x_\jmh^+,t) - \nfl_\jph(\con_h(t)) \basis_l(x_\jph^-,t)
\end{eqnarray*}
where we have introduced the numerical flux
\[
\nfl_\jph(\con_h(t)) = \nfl( \con_\jph^-(t), \con_\jph^+(t), w_\jph(t))
\]
Integrating over the time interval $(t_n, t_{n+1})$ and using (\ref{eq:ortho}), we obtain
\begin{eqnarray*}
h_j^{n+1} \con_{j,l}^{n+1} &=& h_j^n \con_{j,l}^n + \int_{t_n}^{t_{n+1}} \int_{x_\jmh(t)}^{x_\jph(t)} \mfl(\con_h,w) \df{}{x}\basis_l(x,t) \ud x \ud t \\
&&  + \int_{t_n}^{t_{n+1}} [\nfl_\jmh(t) \basis_l(x_\jmh^+,t) - \nfl_\jph(t) \basis_l(x_\jph^-,t)] \ud t
\end{eqnarray*}
The above scheme has an implicit nature since the unknown solution $\con_h$ appears on the right hand side integrals whereas we only know the solution at time $t_n$. In order to obtain an explicit scheme, we assume that we have available with us a {\em predicted solution} $\conp_h$ in the time interval $(t_n,t_{n+1})$, which is used in the time integrals to obtain an explicit scheme
\begin{eqnarray*}
h_j^{n+1} \con_{j,l}^{n+1} &=& h_j^n \con_{j,l}^n + \int_{t_n}^{t_{n+1}} \int_{x_\jmh(t)}^{x_\jph(t)} \mfl(\conp_h,w) \df{}{x}\basis_l(x,t) \ud x \ud t \\
&&  + \int_{t_n}^{t_{n+1}} [\nfl_\jmh(\conp_h(t)) \basis_l(x_\jmh^+,t) - \nfl_\jph(\conp_h(t)) \basis_l(x_\jph^-,t)] \ud t
\end{eqnarray*}
The integrals are computed using quadrature leading to the fully discrete scheme
\begin{eqnarray*}
h_j^{n+1} \con_{j,l}^{n+1} &=& h_j^n \con_{j,l}^n + \Delta t_n \sum_r \wt_r h_j(\tau_r) \sum_q \wx_q\mfl(\conp_h(x_q,\tau_r),w(x_q,\tau_r)) \df{}{x}\basis_l(x_q,\tau_r) \\
&&  + \Delta t_n \sum_r \wt_r [\nfl_\jmh(\conp_h(\tau_r)) \basis_l(x_\jmh^+,\tau_r) - \nfl_\jph(\conp_h(\tau_r)) \basis_l(x_\jph^-,\tau_r)]
\end{eqnarray*}
where $\wt_r$ are weights for time quadrature and $\wx_q$ are weights for spatial quadrature. For the spatial integral, we will use $q=k+1$ point Gauss quadrature. For the time integral we will use mid-point rule for $k=1$ and two point Gauss quadrature for $k=2,3$. Since the mesh is moving,  the spatial quadrature points $x_q$ depend on the quadrature time $\tau_r$ though this is not clear from the notation.
%-------------------------------------------------------------------------------
\subsection{Mesh velocity}
The mesh velocity must be close to the local fluid velocity in order to have a Lagrangian character to the scheme. Since the solution is discontinuous, there is no unique fluid velocity at the mesh boundaries. Some researchers solve a Riemann problem at the cell face to determine the face velocity. Since we use an ALE formulation, we do not require the exact velocity and in our work we make a simple choice which is to take an average
\[
\tilde{w}_\jph^n = \half[ v(x_\jph^-,t_n) + v(x_\jph^+, t_n)]
\]
We will also perform some smoothing of the mesh velocity, e.g., the actual face velocity is computed from
\[
w_\jph^n = \frac{1}{3}( \tilde{w}_{j-\half}^n + \tilde{w}_\jph^n + \tilde{w}_{j+\frac{3}{2}}^n)
\]
%-------------------------------------------------------------------------------
\section{Computing the predictor}
The predicted solution is used to approximate the flux integrals over the time interval $(t_n,t_{n+1})$. Several methods for computing the predictor have been reviewed in~\cite{Gassner20114232}. The simplest approach is to use a Taylor expansion. Since the cells are moving, the Taylor expansion has to be performed along the trajectory of the mesh motion. For a second order scheme, an expansion retaining only linear terms in $t$ and $x$ is sufficient. Consider a quadrature point $(x_q,\tau_r)$; the backward trajectory at time $t_n$ hits the $x$ axis at $X_q = x_q - (\tau_r - t_n)w_q \in [x_\jmh^n,x_\jph^n]$ with $w_q = w(x_q, \tau_r)$. The Taylor expansion around $(X_q,t_n)$ is
\begin{eqnarray*}
\con(x_q,\tau_r) &=& \con(X_q,t_n) + (\tau_r - t_n) \df{\con}{t}(X_q,t_n) + (x_q - X_q) \df{\con}{x}(X_q,t_n) \\
&& + O(\tau_r-t_n)^2 + O(x_q-X_q)^2
\end{eqnarray*}
and hence the predicted solution is 
\[
\conp(x_q,\tau_r) = \con_h(X_q,t_n) + (\tau_r - t_n) \df{\con_h}{t}(X_q,t_n) + (x_q - X_q) \df{\con_h}{x}(X_q,t_n)
\]
Using the conservation law, the time derivative is written as $\df{\con}{t} = -\df{\fl}{x} = - A \df{\con}{x}$ so that predictor is given by
\[
\conp_h(x_q,\tau_r) = \con_h^n(X_q) - (\tau_r - t_n) \left[ A(\con_h^n(X_q)) - w_q I \right] \df{\con_h^n}{x}(X_q)
\]
The above predictor is used for the case of polynomial degree $k=1$. This procedure can be extended to higher orders by including more terms in the Taylor expansion but the algebra becomes complicated. Instead we will adopt the approach of continuous explicit Runge-Kutta (CERK) schemes~\cite{Owren:1992:DEC:141072.141096} to approximate the predictor. 

Let us choose a set of $(k+1)$ distinct nodes, e.g., Gauss-Legendre or Gauss-Lobatto nodes, which uniquely define the polynomial of degree $k$. These nodes are moving with velocity $w(x,t)$, so that the time evolution of the solution at $x=x_m$ is governed by
\begin{eqnarray*}
\dd{\conp_m}{t} &=& \df{}{t}\conp_h(x_m,t) + w(x_m,t) \df{}{x}\conp_h(x_m,t) \\
&=& -\df{}{x}\fl(\conp_h(x_m,t)) + w(x_m,t) \df{}{x} \conp_h(x_m,t) \\
&=& -[ A(\conp_m(t)) - w_m(t) I] \df{}{x}\conp_h(x_m,t) =: \rhs_m(t)
\end{eqnarray*}
with initial condition
\[
\conp_m(t_n) = \con_h(x_m, t_n)
\]
Using a Runge-Kutta scheme of sufficient order (see Appendix), we will approximate the solution at these nodes as
\[
\conp_m(t) = \con_h(x_m, t_n) + \sum_{s=1}^{n_s} b_s((t-t_n)/\Delta t_n) \rhs_{m,s}, \quad t \in [t_n,t_{n+1}), \quad m=0,1,\ldots,k
\]
where $\rhs_{m,s} = \rhs_m(t_n + \tau_s)$ and $\tau_s$ is the stage time. Note that we are evolving the nodal values but the computation of $\rhs_{m,s}$ requires the modal representation of the solution in order to calculate its spatial derivative. 

\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{cerk2}
\end{center}
\caption{Quadrature points for third order scheme}
\label{fig:cerk2}
\end{figure}

Once the predictor is computed as above, it must be evaluated at the quadrature point $(x_q, \tau_r)$ as follows. For each time quadrature point $\tau_r \in [t_n, t_{n+1}]$, 
\begin{enumerate}
\item Compute nodal values  $\conp_m(\tau_r)$, $m=0,1,\ldots,k$
\item Convert nodal values to modal coefficients $\con_{m,r}$, $m=0,1,\ldots,k$
\item Evaluate predictor $\conp_h(x_q,\tau_r) = \sum_{m=0}^k \con_{m,r} \basis(x_q, \tau_r)$
\end{enumerate}
The predictor is also computed at the cell boundaries using the above procedure. Figure~(\ref{fig:cerk2}) shows the quadrature points used in the third order scheme together with CERK2 for the predictor.
%-------------------------------------------------------------------------------
\section{Limiter}
High order schemes for hyperbolic problem suffer from spurious numerical oscillations when discontinuities are present in the solution. In the case of scalar problems, this is a manifestation of loss of TVD property and hence limiters are used to satisfy some form of TVD condition. In the case of DG schemes, the limiter is used as a post processor which is applied on the solution after the time update has been performed.
%-------------------------------------------------------------------------------
\section{Positivity property}
The solutions of Euler equations are well defined only if the density and pressure are positive quantities. This is not a priori guaranteed by the DG scheme even when the TVD limiter is applied. In the case of Runge-Kutta DG schemes, a positivity limiter has been developed in~\cite{Zhang:2010:PHO:1864819.1865066} which preserves accuracy in smooth regions. This scheme is built on a positive first order finite volume scheme. Consider the first order version of the ALE-DG scheme which is a finite volume scheme given by
\begin{equation}
h_j^{n+1} \con_j^{n+1} = h_j^n \con_j^n - \Delta t_n [ \nfl_\jph^n - \nfl_\jmh^n]
\label{eq:fvm}
\end{equation}
We will analyze the positivity of this scheme for the case of Rusanov flux which is given in section~\ref{sec:rusanov}. The update equation can be rewritten as
\begin{eqnarray*}
h_j^{n+1} \con_j^{n+1} &=& \left[ h_j^n - \frac{\Delta t_n}{2}( \lambda_\jmh^n + w_\jmh^n + \lambda_\jph^n - w_\jph^n) \right] \con_j^n \\
&& + \frac{\Delta t_n}{2} \left[ (\lambda_\jmh^n - w_\jmh^n) \con_{j-1}^n + \fl_{j-1}^n  \right] \\
&& + \frac{\Delta t_n}{2} \left[ (\lambda_\jph^n + w_\jph^n) \con_{j+1}^n - \fl_{j+1}^n \right] \\
&=& I + II + III
\end{eqnarray*}
We can easily show that
\[
(\lambda_\jmh - w_\jmh) + v_{j-1} \ge c_{j-1} > 0, \qquad (\lambda_\jph + w_\jph) - v_{j+1} \ge c_{j+1} > 0
\]
Consider the first component of $II$
\begin{eqnarray*}
(\lambda_\jmh - w_\jmh) \rho_{j-1} + \rho_{j-1} v_{j-1} &\ge& (|v_j - w_\jmh| + c_{j-1} - w_{j-1} + v_{j-1}) \rho_{j-1} \\
&\ge& c_{j-1} \rho_{j-1} > 0
\end{eqnarray*}
Consider the first component of $III$
\begin{eqnarray*}
(\lambda_\jph + w_\jph) \rho_{j+1} - \rho_{j+1} v_{j+1} &\ge& (|v_j - w_\jph| + c_{j+1} + w_{j-1} - v_{j+1}) \rho_{j+1} \\
&\ge& c_{j+1} \rho_{j+1} > 0
\end{eqnarray*}
The pressure corresponding to $II$ is
\[
p_{j-1} \left( -p_{j-1} + \frac{2\rho_{j-1} (v_{j-1} + \lambda_\jmh - w_\jmh)^2}{\gamma-1} \right)
\]
Hence if the coefficient in term $I$ is positive, then the scheme is positive. This requires the CFL condition
\[
\Delta t_n \le \frac{2h_j^n}{\lambda_\jmh^n + w_\jmh^n + \lambda_\jph^n - w_\jph^n}
\]
The time step will also be restricted to ensure that the cell size does not change too much in one time step. If we demand that the cell size does not change by more than a fraction $\beta \in (0,1)$, then we need to ensure that
\[
\Delta t_n \le \frac{\beta h_j^n}{|w_\jph^n - w_\jmh^n|}
\]
Combining the previous two conditions, we obtain the following condition on the time step
\begin{equation}
\Delta t_n \le \Delta t_n^{(1)} := \min_j\left\{ \frac{(1-\thalf\beta) h_j^n}{\thalf(\lambda_\jmh^n + \lambda_\jph^n)},  \frac{\beta h_j^n}{|w_\jph^n - w_\jmh^n|} \right\}
\label{eq:dtpos}
\end{equation}
We can now state the following result on the positivity of the first order finite volume scheme on moving meshes.
%-------------------------------------------------------------------------------
\begin{theorem}
The scheme~(\ref{eq:fvm}) with Rusanov flux is positivity preserving if the time step condition~(\ref{eq:dtpos}) is satisfied.
\end{theorem}

\begin{remark} 
In this work we have not attempted to prove the positivity of the scheme for other numerical fluxes. We also do not have a proof of positivity for higher order version of the scheme. In the computations, we use the positivity preserving limiter of~\cite{Zhang:2010:PHO:1864819.1865066} which leads to robust schemes which preserve the positivity.
\end{remark}
%-------------------------------------------------------------------------------
\section{Grid coarsening and refinement}
The size of the cells can change considerably during the time evolution process due to the near Lagrangian movement of the cell boundaries. Near shocks, the cells will be compressed to smaller sizes which will reduce the allowable time step since a CFL condition has to be satisfied. In some regions, e.g., inside expansion fans, the cell size can increase considerably which may lead to loss of accuracy.
%-------------------------------------------------------------------------------
\section{Linear advection equation}
Consider the application of the proposed ALE-DG scheme to the linear advection equation $u_t + a u_x=0$. In this case the mesh velocity is equal to the advection velocity $w_\jph = a$, i.e., the cells move along the characteristics. This implies that $g(u,w)=au-wu=0$ and $\hat{g}_\jph = 0$. Thus the DG scheme reduces to
\[
\int_{x_\jmh^{n+1}}^{x_\jph^{n+1}} u_h(x,t) \basis(x,t_{n+1}) \ud x = \int_{x_\jmh^n}^{x_\jph^n} u_h(x,t) \basis(x,t_{n}) \ud x
\]
so that the solution at time $t_n$ has been advected exactly to the solution at time $t_{n+1}$. Note that there is no time step restriction involved here.
%-------------------------------------------------------------------------------
\section{Numerical results}
The numerical tests are performed with polynomials of degree one, two and three. When the spatial polynomial degree is 1, 2, 3, we use the linear Taylor expansion, two stage CERK and four stage CERK, respectively, for the predictor. For the quadrature in time, we use the mid-point rule, two and three point Gauss-Legendre quadrature, respectively. The time step is chosen based on the CFL condition
\[
\Delta t_n = \frac{\cfl}{2k+1} \Delta t_n^{(1)}
\]
and in most of the computations we use $\cfl = 0.9$. The main steps in the scheme are as follows.
\begin{enumerate}
\item Update solution $\con_h^n$ to the next time level $\con_h^{n+1}$
\item Apply TVD/TVB limiter
\item Apply positivity limiter
\end{enumerate}
%-------------------------------------------------------------------------------
\subsection{Order of accuracy}
We study the convergence rate of the schemes by applying them to a problem with smooth solution. The initial condition is taken as
\[
\rho(x,0) = 1 + \exp(-10 x^2), \qquad u(x,0) = 1, \qquad p(x,0) = 1
\]
whose exact solution is $\rho(x,t) = \rho(x-t,0)$, $u(x,t)=1$, $p(x,t)=1$. The initial domain is $[-5,+5]$ and the final time is $t=1$ units. The $L^2$ norm of the error in density are shown in table~(\ref{tab:ord0}) for the static mesh and in table~(\ref{tab:ord1}) for the moving mesh. In each case, we see that the error behaves as $O(h^{k+1})$ which is the optimal rate for smooth solutions. For moving mesh also, the convergence rate is computed based on initial mesh spacing.
\begin{table}
\begin{tabular}{|l |c|c|c|c|c|c|}
\hline
NC & Taylor Error & Rate & CERK2 Error & Rate & CERK3 Error & Rate \\
\hline
100 &4.37049005E-02& &3.49858228E-03 &&3.88303371E-04& \\
200 &6.61190672E-03 &2.725 &4.76618893E-04 &2.876 &1.62018261E-05 &4.583 \\
400 &1.33247692E-03 &2.518 &6.41518492E-05 &2.885 &9.37686544E-07 &4.347\\
800 &3.15102750E-04 &2.372 &8.24679617E-06 &2.910 &5.76310379E-08 &4.239 \\
1600 &7.84612564E-05 &2.280 &1.03117601E-06 &2.932 &3.59501919E-09 &4.180\\
\hline
\end{tabular}
\caption{Order of accuracy study on static mesh}
\label{tab:ord0}
\end{table}

\begin{table}
\begin{tabular}{|l |c|c|c|c|c|c|}
\hline
NC & Taylor Error & Rate & CERK2 Error & Rate & CERK3 Error & Rate \\
\hline
100&2.33157046E-02&&3.97925687E-03&&8.63374760E-04& \\
200&6.13960535E-03&1.925&4.05829467E-04&3.294&1.18596922E-05& 6.186 \\
400&1.40601665E-03&2.0258&5.25052534E-05&3.122&7.07979382E-07& 5.126 \\
800&3.37577081E-04&2.0366&6.62679723E-06&3.077&4.34048259E-08& 4.760 \\
1600&8.27898213E-05&2.0344&8.30445619E-07&3.057&2.68982550E-09& 4.573\\
\hline
\end{tabular}
\caption{Order of accuracy study on  moving mesh}
\label{tab:ord1}
\end{table}
%-------------------------------------------------------------------------------
\subsection{Sod problem}
The initial condition is given by
\[
(\rho,v,p) = \begin{cases}
(1.0, 0.0, 1.0) & x < 0.5 \\
(0.125, 0.0, 0.1) & x > 0.5
\end{cases}
\]
$T=0.2$
\begin{figure}
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=0.48\textwidth]{sod_100_static} &
\includegraphics[width=0.48\textwidth]{sod_100_moving} \\
(a) & (b)
\end{tabular}
\end{center}
\caption{Sod problem using 100 cells and TVD limiter: (a) static mesh (b) moving mesh}
\end{figure}
%-------------------------------------------------------------------------------
\subsection{Lax problem}
The initial condition is given by
\[
(\rho,v,p) = \begin{cases}
(0.445, 0.698, 3.528) & x < 0 \\
(0.5, 0.0, 0.571) & x > 0
\end{cases}
\]
$T=1.3$
%-------------------------------------------------------------------------------
\subsection{Shu-Osher problem}
The initial condition is given by
\[
(\rho,v,p) = \begin{cases}
(3.857143, 2.629369, 10.333333) & x < -4 \\
(1+0.2\sin(5x), 0.0, 1.0) & x > -4
\end{cases}
\]
$T=1.8$
\begin{figure}
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=0.48\textwidth]{shuosher_200_M0_static} &
\includegraphics[width=0.48\textwidth]{shuosher_200_M0_moving} \\
(a)   & (b)   \\
\includegraphics[width=0.48\textwidth]{shuosher_200_M100_static} &
\includegraphics[width=0.48\textwidth]{shuosher_300_M100_static} \\
(c) & (d)
\end{tabular}
\end{center}
\caption{Shu-Osher problem: (a) static mesh, 200 cells, $M=0$ (b) moving mesh, 200 cells, $M=0$ (c) static mesh, 200 cells, $M=100$ (d) static mesh, 300 cells, $M=100$}
\end{figure}
%-------------------------------------------------------------------------------
\subsection{Low density problem}
The initial condition is given by
\[
(\rho,v,p) = \begin{cases}
(1.0, -2.0, 0.4) & x < 0.5 \\
(1.0, +2.0, 0.4) & x > 0.5
\end{cases}
\]
and the final is $T=0.15$
%-------------------------------------------------------------------------------
\subsection{Blast problem}
The initial condition is given by
\[
(\rho,v,p) = \begin{cases}
(1.0, 0.0, 1000.0) & x < 0.1 \\
(1.0, 0.0, 0.01) & 0.1 < x < 0.9 \\
(1.0, 0.0, 100.0) & x > 0.9
\end{cases}
\]
and the final time is $T=0.038$.
%-------------------------------------------------------------------------------
\section{Summary and Conclusions}
%-------------------------------------------------------------------------------
\appendix
%-------------------------------------------------------------------------------
\section{Numerical flux}
The ALE scheme requires a numerical flux $\nfl(\con_l,\con_r,w)$ which is usually based on some approximate Riemann solver. Since the ALE versions of the numerical fluxes are not so well known, here we list the formulae used in the present work.
%-------------------------------------------------------------------------------
\subsection{Rusanov flux}
\label{sec:rusanov}
The Rusanov flux is a variant of the Lax-Friedrich flux and is given by
\[
\nfl(\con_l,\con_r,w) = \half[ \mfl(\con_l,w) + \mfl(\con_r,w)] - \half \lambda (\con_r - \con_l)
\]
where $\lambda$ is given by
\[
\lambda = \max\{ |v_l - w|+c_l, |v_r - w|+c_r \}
\]
which is an estimate of the largest wave speed in the Riemann problem.
%-------------------------------------------------------------------------------
\subsection{Roe flux}
The Roe scheme~\cite{Roe1981357} is based on a local linearization of the conservation law and solving a Riemann problem for the linear approximation. The flux can be written as
\[
\nfl(\con_l,\con_r,w) = \half[ \mfl(\con_l,w) + \mfl(\con_r,w)] - \half |A_w| (\con_r - \con_l)
\]
where the Roe average matrix $A_w$ satisfies
\[
\mfl(\con_r,w) - \mfl(\con_l,w) = A_w (\con_r - \con_l)
\]
and $|A_w| = R |\Lambda -wI| R^{-1}$. This matrix is evaluated at the Roe average state $\con(\bar{\pv})$, $\bar{\pv}=\half(\pv_l + \pv_r)$, where $\pv=\sqrt{\rho}[1, \ v, \ H]^\top$ is the parameter vector introduced by Roe.
%-------------------------------------------------------------------------------
\subsection{HLLC flux}
This is based on a three wave approximate Riemann solver and the particular version we use can also be found in~\cite{Luo2004304}. Define the relative velocity $\rv = v - w$; then the numerical flux is given by
\[
\nfl(\con_l,\con_r,w) = \begin{cases}
\mfl(\con_l,w) & S_l > 0 \\
\mfl^*(\con_l^*,w) & S_l \le 0 < S_M \\
\mfl^*(\con_r^*,w) & S_M \le 0 \le S_r \\
\mfl(\con_r,w) & S_r < 0
\end{cases}
\]
where the intermediate states are given by
\[
\con_\alpha^* = \frac{1}{S_\alpha - S_M} \begin{bmatrix}
(S_\alpha - \rv_\alpha) \rho_\alpha \\
(S_\alpha - \rv_\alpha)(\rho v)_\alpha + p^* - p_\alpha \\
(S_\alpha - \rv_\alpha) E_\alpha - p_\alpha \rv_\alpha + p^* S_M
\end{bmatrix}, \qquad \alpha=l,r
\]
and
\[
\mfl^*(\con,w) = S_M \con + \begin{bmatrix}
0 \\
p^* \\
(S_M + w) p^* \end{bmatrix}
\]
where
\[
p^* = \rho_l (\rv_l - S_l) (\rv_l - S_M) + p_l = \rho_r (\rv_r - S_r)(\rv_r - S_M) + p_r
\]
which gives $S_M$ as
\[
S_M = \frac{ \rho_r \rv_r (S_r - \rv_r) - \rho_l \rv_l (S_l - \rv_l) + p_l - p_r}{\rho_r (S_r - \rv_r) - \rho_l (S_l - \rv_l)}
\]
The signal velocities are defined as
\[
S_l = \min\{ \rv_l - c_l, \hat{v}-w-\hat{c}\}, \qquad S_r = \max\{ \rv_r + c_r, \hat{v}-w+\hat{c}\}
\]
where $\hat{v}$, $\hat{c}$ are Roe's average velocity and speed of sound.
%-------------------------------------------------------------------------------
\subsection{HLL-CPS flux}
This is based on the HLL-CPS-Z(Roe) numerical flux of Mandal and Panwar~\cite{Mandal2012148} which we modify for the ALE case. The Lagrangian flux can be written as a convective and pressure flux
\[
\mfl = \rv \begin{bmatrix}
\rho  \\
\rho v  \\
E  \end{bmatrix} + 
\begin{bmatrix}
0 \\
p \\
pv \end{bmatrix} =: \mfl_1 + \mfl_2
\]
where $\rv = v - w$ is the relative velocity. The numerical flux $\nfl = \nfl_1 + \nfl_2$ is given in two parts. The convective numerical flux is given by
\[
\nfl_1 = \begin{cases}
\left(\frac{\rv_l - S_l}{\arv - S_l}\right) \arv \con_l & \mbox{if} \quad \arv \ge 0 \\
\left(\frac{\rv_r - S_r}{\arv - S_r}\right) \arv \con_r & \mbox{if} \quad \arv < 0
\end{cases}, \qquad \mbox{where} \quad \arv = \frac{1}{2}(q_l + q_r)
\]
The pressure numerical flux is given by
\[
\nfl_2 = \frac{1}{2}[ (\mfl_2)_l + (\mfl_2)_r] + \delta \con_2
\]
where
\[
\delta \con_2 = \frac{S_r + S_l}{2(S_r - S_l)}[ (\mfl_2)_l - (\mfl_2)_r] - \frac{S_l S_r}{\ac^2 (S_r - S_l)} \begin{bmatrix}
p_l - p_r \\
(pv)_l - (pv_r) \\
\frac{\ac^2}{\gamma-1}(p_l - p_r) + \frac{1}{2}[(pv^2)_l - (pv^2)_r]
\end{bmatrix}
\]
with $\ac = \frac{1}{2}(c_l + c_r)$. The wave speeds are estimated as
\[
S_l = \min\{ 0, \rv_l - c_l, \hat{v}-w-\hat{c}\}, \qquad S_r = \max\{ 0, \rv_r + c_r, \hat{v}-w+\hat{c}\}
\]
where $\hat{v}$, $\hat{c}$ are Roe's average velocity and speed of sound. If $\arv = 0$, then the speeds are taken as $S_l = -\ac$ and $S_r = \ac$.
%-------------------------------------------------------------------------------

\section{Continuous expansion Runge-Kutta (CERK) schemes}
In this section, we list down the CERK scheme for the following ODE
\[
\dd{u}{t} = f(u,t)
\]
The CERK scheme gives a polynomial solution in the time interval $[t_n, t_{n+1}]$ of the form
\[
u(t_n + \theta h) = u^n + h \sum_{s=1}^{n_s} b_s(\theta) k_s, \qquad \theta \in [0,1]
\]
where $n_s$ is the number of stages and $h$ denotes the time step.

\subsection{Second order (CERK2)}
The number of stages is $n_s = 2$ and
\[
b_1(\theta) = \theta - \theta^2/2, \qquad b_2(\theta) = \theta^2/2
\]
and
\begin{eqnarray*}
k_1 &=& f(u^n, t_n) \\
k_2 &=& f(u^n + h k_1, t_n + h)
\end{eqnarray*}

\subsection{Third order (CERK3)}
The number of stages is $n_s = 4$ and
\begin{eqnarray*}
k_1 &=& f(u^n, t_n) \\
k_2 &=& f(u^n + (12/23) h k_1, t_n + 12 h /23) \\
k_3 &=& f(u^n + h ((-68/375) k_1 + (368/375) k_2), t_n + 4h/5) \\
k_4 &=& f(u^n + h ((31/144) k_1 + (529/1152) k_2 + (125/384) k_3), t_n + h)
\end{eqnarray*}
and
\begin{eqnarray*}
b_1(\theta) &=& (41/72) \theta^3 - (65/48) \theta^2 + \theta \\
b_2(\theta) &=& -(529/576) \theta^3 + (529/384) \theta^2 \\
b_3(\theta) &=& -(125/192) \theta^3 + (125/128) \theta^2 \\
b_4(\theta) &=& \theta^3 - \theta^2
\end{eqnarray*}
%-------------------------------------------------------------------------------
\section*{References}

\bibliographystyle{siam}
\bibliography{bibdesk}
\end{document}  